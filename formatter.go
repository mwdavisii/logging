package logging

import (
	"bytes"
	"fmt"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
)

const (
	// This is the caller depth, that passes through the logging facade
	// If the facade is moved or wrapped, this will need to adjust
	callerFrameDepth       = 9
	defaultTimeStampFormat = "2006-01-02 15:04:05"
)

var (
	baseFilePath, _ = filepath.Abs("")
)

//LogFormat is the struct to hold the TimestampFormat
type LogFormat struct {
	TimestampFormat string
}

//Format actually formats the log messages
func (f *LogFormat) Format(entry *logrus.Entry) ([]byte, error) {
	var b *bytes.Buffer

	b = f.initializeBuffer(entry)

	f.addLogDetails(entry, b)
	f.addMethodDetails(entry, b, callerFrameDepth)

	// Message is formatted as - message
	if entry.Message != "" {
		formattedMessage := fmt.Sprintf(" - %v", entry.Message)
		b.WriteString(formattedMessage)
	}

	f.addData(entry, b)

	b.WriteByte('\n')
	return b.Bytes(), nil
}

func (f *LogFormat) initializeBuffer(entry *logrus.Entry) (b *bytes.Buffer) {
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	return b
}

func (f *LogFormat) addLogDetails(entry *logrus.Entry, b *bytes.Buffer) {
	// Level will be formatted as [INFO]:
	formattedLevel := fmt.Sprintf("[%-9v", strings.ToUpper(entry.Level.String()+"]"))
	b.WriteString(formattedLevel)

	// Time is formatted as 01-02-2006 15:04:05
	b.WriteString(entry.Time.Format(f.TimestampFormat) + " ")
}

func (f *LogFormat) addMethodDetails(entry *logrus.Entry, b *bytes.Buffer, callDepth int) {
	caller := entry.Caller
	methodName := caller.Function

	callerFilePath, _ := filepath.Rel(baseFilePath, caller.File)

	formattedFile := fmt.Sprintf("[%v] ", callerFilePath)
	b.WriteString(formattedFile)

	methodChunks := strings.Split(methodName, "/")
	formattedMethod := fmt.Sprintf("[%v: %v]", methodChunks[len(methodChunks)-1], caller.Line)
	b.WriteString(formattedMethod)
}

func (f *LogFormat) addData(entry *logrus.Entry, b *bytes.Buffer) {
	if len(entry.Data) > 0 {
		b.WriteString(" || ")
	}

	for key, value := range entry.Data {
		formattedData := fmt.Sprintf("%v={ %v }, ", key, value)
		b.WriteString(formattedData)
	}
}
