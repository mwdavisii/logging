package logging

import (
	"context"
	"fmt"
	"io"
	"os"

	"github.com/sirupsen/logrus"
)

const (
	loggingPermissions = 0644
	defaultLogLevel    = "error"
)

//Config is responsible for setting up the logging environment.
type Config struct {
	FileLocation   string
	Level          string
	ConsoleEnabled bool
}

//Initialize is the initialization func
func (c *Config) Initialize(ctx context.Context) error {
	logFile, err := c.openLogFile()
	if err != nil {
		return err
	}

	c.setOutput(logFile)
	c.setLogLevel()

	Log.Info("Logging initialized")

	return nil
}

func (c *Config) openLogFile() (logFile *os.File, err error) {
	logFile, err = os.OpenFile(c.FileLocation, os.O_WRONLY|os.O_APPEND|os.O_CREATE, loggingPermissions)
	if err != nil {
		formattedErr := fmt.Errorf("failed to open log file (%v) for logging: %v", c.FileLocation, err)
		Log.Error(formattedErr.Error())

		return nil, formattedErr
	}

	return logFile, nil
}

func (c *Config) setOutput(logFile *os.File) {
	var writer io.Writer
	if c.ConsoleEnabled {
		writer = io.MultiWriter(os.Stdout, logFile)
	} else {
		writer = io.Writer(logFile)
	}

	Log.SetOutput(writer)
}

func (c *Config) setLogLevel() {
	logLevel, err := logrus.ParseLevel(c.Level)
	if err != nil {
		Log.Errorf("incorrectly formatted log level %v, setting log level to %s", c.Level, defaultLogLevel)

		logLevel, _ = logrus.ParseLevel(defaultLogLevel)
	}

	Log.SetLevel(logLevel)
}
