package logging

import (
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"io"
	"os"
	"testing"
)

const (
	testLogFileLocation = "temp_server.log"
	testLevel           = "info"
)

var (
	testLoggingConfig = Config{
		FileLocation:   testLogFileLocation,
		Level:          testLevel,
		ConsoleEnabled: false,
	}
)

func TestOpenLogFile(t *testing.T) {
	_, err := testLoggingConfig.openLogFile()
	defer os.Remove(testLogFileLocation)

	assert.Nil(t, err, "openLogFile(): Unexpected error opening log file %v: %v", testLogFileLocation, err)
	assert.FileExists(t, testLogFileLocation, "openLogFile(): Log file not created")
}

func TestSetOutputConsoleDisabled(t *testing.T) {
	testLogFile := &os.File{}
	defer tearDown()

	testLoggingConfig.setOutput(testLogFile)

	assert.Equal(t, io.Writer(testLogFile), Log.Out, "testSetOutput(): writers do not match")
}

func TestSetOutputConsoleEnabled(t *testing.T) {
	testLogFile := &os.File{}

	testLoggingConfig.ConsoleEnabled = true
	defer tearDown()

	testLoggingConfig.setOutput(testLogFile)

	assert.IsType(t, io.MultiWriter(), Log.Out, "testSetOutput(): writers do not match")
}

func TestSetLogLevel(t *testing.T) {
	logLevel, _ := logrus.ParseLevel(testLevel)

	testLoggingConfig.setLogLevel()

	assert.Equal(t, logLevel, Log.GetLevel(), "setLogLevel(): log levels do not match")
}

func TestSetLogLevelWithInvalidLevel(t *testing.T) {
	expectedLogLevel, _ := logrus.ParseLevel(defaultLogLevel)

	testLoggingConfig.Level = "invalid level"
	defer (func() {
		testLoggingConfig.Level = "info"
	})()

	testLoggingConfig.setLogLevel()

	assert.Equal(t, expectedLogLevel, Log.GetLevel(), "setLogLevel(): invalid level did not use default log level")
}

func tearDown() {
	testLoggingConfig.ConsoleEnabled = false
	testLoggingConfig.setOutput(os.Stdout)
}
