package logging

import (
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestNewLogger(t *testing.T) {
	newLogger := NewLogger()

	expectedLevel, _ := logrus.ParseLevel(defaultLogLevel)
	expectedFormatter := &LogFormat{
		TimestampFormat: defaultTimeStampFormat,
	}

	assert.Equal(t, os.Stdout, newLogger.Out, "NewLogger(): The default out does not match stdout")
	assert.Equal(t, expectedLevel, newLogger.Level, "NewLogger(): The default level is not correctly set")
	assert.Equal(t, expectedFormatter, newLogger.Formatter, "NewLogger(): The formatter is not properly set")
}
