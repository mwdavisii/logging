package logging

import (
	"bytes"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"runtime"
	"testing"
	"time"
)

var (
	testTime, _       = time.Parse(defaultTimeStampFormat, "2006-01-02 15:04:05")
	testEntryLevel, _ = logrus.ParseLevel("info")
	testMessage       = "This is a test message"
	testEntry         = &logrus.Entry{
		Data:    nil,
		Time:    testTime,
		Level:   testEntryLevel,
		Caller:  nil,
		Message: testMessage,
		Buffer:  nil,
	}
	testLogFormat = &LogFormat{
		TimestampFormat: defaultTimeStampFormat,
	}
)

func TestInitializeBufferWithEmptyBuffer(t *testing.T) {
	actualBuffer := testLogFormat.initializeBuffer(testEntry)

	assert.Empty(t, actualBuffer.String(), "initializeBuffer(): The buffer should be a new empty buffer with no buffer in an entry")
}

func TestInitializeBufferWithNonEmptyBuffer(t *testing.T) {
	expectedBuffer := &bytes.Buffer{}
	expectedBufferString := "Buffer is not empty"
	expectedBuffer.WriteString(expectedBufferString)

	testEntry.Buffer = expectedBuffer
	defer func() {
		testEntry.Buffer = &bytes.Buffer{}
	}()

	actualBuffer := testLogFormat.initializeBuffer(testEntry)

	assert.Equal(t, expectedBufferString, actualBuffer.String(), "initializeBuffer(): The buffer should match the expected buffer")
}

func TestAddLogDetails(t *testing.T) {
	testBuffer := &bytes.Buffer{}

	testLogFormat.addLogDetails(testEntry, testBuffer)

	assert.Equal(t, "[INFO]    2006-01-02 15:04:05 ", testBuffer.String(), "addLogDetails(): The output details did not match expected details")
}

func TestAddMethodDetails(t *testing.T) {
	testBuffer := &bytes.Buffer{}

	caller, _, lineNumber, _ := runtime.Caller(0)
	callerFrame, _ := runtime.CallersFrames([]uintptr{caller}).Next()
	testEntry.Caller = &callerFrame

	testLogFormat.addMethodDetails(testEntry, testBuffer, 1)

	expectedEntry := fmt.Sprintf("[formatter_test.go] [logging.TestAddMethodDetails: %d]", lineNumber)
	assert.Equal(t, expectedEntry, testBuffer.String(), "addMethodDetails(): The method and file were not as expected")
}

func TestAddDataWithEmptyData(t *testing.T) {
	testBuffer := &bytes.Buffer{}

	testLogFormat.addData(testEntry, testBuffer)

	assert.Empty(t, testBuffer.String(), "addData(): The buffer should be empty with no data in an entry")
}

func TestAddDataWithData(t *testing.T) {
	testBuffer := &bytes.Buffer{}

	testKey := "testKey"
	testVal := "testVal"
	testEntry = testEntry.WithField(testKey, testVal)

	expected := fmt.Sprintf(" || %v={ %v }, ", testKey, testVal)

	testLogFormat.addData(testEntry, testBuffer)

	assert.Equal(t, expected, testBuffer.String(), "addData(): The buffer should be empty with no data in an entry")
}
