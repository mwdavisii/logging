package logging

import (
	"os"

	"github.com/sirupsen/logrus"
)

//Log is the declared value of a new logger instance
var (
	Log = NewLogger()
)

//NewLogger is the initialization method for logrus
func NewLogger() *logrus.Logger {
	level, _ := logrus.ParseLevel(defaultLogLevel)
	logger := &logrus.Logger{
		Out:          os.Stdout,
		Level:        level,
		ReportCaller: true,
		Formatter: &LogFormat{
			TimestampFormat: defaultTimeStampFormat,
		},
	}
	return logger
}
